const { I } = inject();

module.exports = {
  fields:{
    fieldUser: '#user',
    fieldEmail: '#email',
    fieldPassword: '#password'
  },
  buttons:{
    btnRegister: '#btnRegister'
  },
  messages:{
    registerSuccess: 'Cadastro realizado!',
    welcomeUser: 'Bem-vindo ',
    emptyField: 'O campo nome deve ser prenchido',
    invalidEmail: 'O campo e-mail deve ser prenchido corretamente',
    invalidPassword: 'O campo senha deve ter pelo menos 6 dígitos'
  },
  register(username, email, password){
    I.fillField(this.fields.fieldUser, username)
    I.fillField(this.fields.fieldEmail, email)
    I.fillField(this.fields.fieldPassword, password)
    I.click(this.buttons.btnRegister)
  }
}
