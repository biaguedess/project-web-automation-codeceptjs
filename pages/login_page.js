const { I } = inject();

module.exports = {

  fields:{
    fieldUser: '#user',
    fieldPassword: '#password'
  },

  buttons:{
    btnLogin: '#btnLogin'
  },

  messages:{
    loginSuccess: 'Login realizado',
    invalidEmail: 'E-mail inválido.',
    invalidPassword: 'Senha inválida.'
  },

  // insert your locators and methods here
  login(user, password) {
    I.fillField(this.fields.fieldUser, user)
    I.fillField(this.fields.fieldPassword, password)
    I.click(this.buttons.btnLogin)
  }
}
