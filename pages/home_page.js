const { I } = inject();

module.exports = {

  buttons:{
    btnLogin: 'Login',
    btnCadastro:'Cadastro'
  },

  // insert your locators and methods here
  accessLogin(){
    I.click(this.buttons.btnLogin)
    I.wait(5)
  },

  accessCadastro(){
    I.click(this.buttons.btnCadastro)
    I.wait(5)
  }
}
