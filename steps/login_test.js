const {I, home_page, login_page} = inject()
var faker = require('faker')

Feature('Login User');

Before(() => {
  I.amOnPage('/')
  home_page.accessLogin()
})

Scenario('Login with success', () => {
    
    email = faker.internet.email()
    login_page.login(email, secret(faker.internet.password(6, false, /^[0-9]*$/)))

    I.see(login_page.messages.loginSuccess)
    I.see(email)

})

Scenario('Login without success - empty fields', () => {
  login_page.login('', '')
  I.see(login_page.messages.invalidEmail)
})

Scenario('Login without success - invalid email format', () => {
  login_page.login('teste', '')
  I.see(login_page.messages.invalidEmail)

  login_page.login('teste@teste', '')
  I.see(login_page.messages.invalidEmail)

  login_page.login('@teste.com', '')
  I.see(login_page.messages.invalidEmail)
})

Scenario('Login without success - empty password', () => {
  login_page.login('teste@teste.com', '')
  I.see(login_page.messages.invalidPassword)
})

Scenario('Login without success - invalid password format', () => {
  login_page.login('teste@teste.com', secret('12345'))
  I.see(login_page.messages.invalidPassword)
})