const {I, home_page, register_page} = inject()
var faker = require('faker')

Feature('Register User');

Before(() => {
    I.amOnPage('/')
    home_page.accessCadastro()
})

Scenario('Register with success', () => {
    
    username = faker.name.firstName() + ' ' + faker.name.lastName()
    email = faker.internet.email()
    console.log(username + email)
    register_page.register(username, email, secret(faker.internet.password(6, false, /^[0-9]*$/)))
    I.see(register_page.messages.registerSuccess)
    I.see(register_page.messages.welcomeUser + username)

})

Scenario('Register without success - empty fields - all', () => {    
    register_page.register('', '', '')
    I.see(register_page.messages.emptyField)
})

Scenario('Register without success - empty name and password', () => {
    register_page.register('', 'teste@teste.com', '')
    I.see(register_page.messages.emptyField)
})

Scenario('Register without success - empty name and email', () => {
    register_page.register('', '', secret('123456'))
    I.see(register_page.messages.emptyField)    
})

Scenario('Register without success - empty email', () => {
    register_page.register('Aline Soares', '', secret('123456'))
    I.see(register_page.messages.invalidEmail)        
})

Scenario('Register without success - invalid email format', () => {
    register_page.register('Aline Soares', 'teste', secret('123456'))
    I.see(register_page.messages.invalidEmail)    

    register_page.register('Aline Soares', 'teste@teste', secret('123456'))
    I.see(register_page.messages.invalidEmail)  

    register_page.register('Aline Soares', '@teste.com', secret('123456'))
    I.see(register_page.messages.invalidEmail)  

})

Scenario('Register without success - empty password', () => {
    register_page.register('Aline Soares', 'teste@teste.com', '')
    I.see(register_page.messages.invalidPassword)  
})

Scenario('Register without success - invalid password', () => {
    register_page.register('Aline Soares', 'teste@teste.com', secret('12345'))
    I.see(register_page.messages.invalidPassword)  
})